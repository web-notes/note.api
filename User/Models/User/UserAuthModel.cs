﻿using Newtonsoft.Json;

namespace User.Models.User
{
    public class UserAuthModel
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }
}

﻿using Newtonsoft.Json;

namespace Note.Models.Note
{
    public class NoteModel
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("modified")]
        public DateTime Modified { get; set; }
    }
}

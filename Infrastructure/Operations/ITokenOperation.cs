﻿using System.Security.Claims;

namespace Infrastructure.Operations
{
    public interface ITokenOperation
    {
        string Generate(ClaimsIdentity identity, out DateTime expirationDate);
    }
}

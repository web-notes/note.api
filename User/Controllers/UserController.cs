﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Infrastructure.Operations;
using Infrastructure.Repositories;
using User.Models.User;
using User.Interfaces.Operations;

namespace User.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IIdentityOperation identityOperation;

        private readonly ITokenOperation tokenOperation;

        private readonly IUserRepository userRepository;

        private readonly IHashOperation hashOperation;

        public UserController(IIdentityOperation identityOperation, ITokenOperation tokenOperation, IUserRepository userRepository, IHashOperation hashOperation)
        {
            this.identityOperation = identityOperation;
            this.tokenOperation = tokenOperation;
            this.userRepository = userRepository;
            this.hashOperation = hashOperation;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("login")]
        public TokenModel Login(UserAuthModel model)
        {
            var passwordHash = hashOperation.Hash(model.Password);

            var identity = identityOperation.Get(model.Name, passwordHash);
            var token = tokenOperation.Generate(identity, out var expirationDate);

            return new TokenModel { AccessToken = token, ExpirationDate = expirationDate };
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("register")]
        public HttpResponseMessage Register(UserRegisterModel model)
        {
            var passwordHash = hashOperation.Hash(model.Password);

            userRepository.Create(model.Name, passwordHash);

            return new HttpResponseMessage(System.Net.HttpStatusCode.OK);
        }
    }
}

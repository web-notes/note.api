﻿using Infrastructure.Entities;
using Infrastructure.Repositories;
using Infrastructure.Operations;

namespace Operation.Note
{
    public class NoteOperation : INoteOperation
    {
        private readonly INoteRepository noteRepository;

        private readonly ICryptoOperation cryptoOperation;

        public NoteOperation(INoteRepository noteRepository, ICryptoOperation cryptoOperation)
        {
            this.cryptoOperation = cryptoOperation;
            this.noteRepository = noteRepository;
        }

        public INote Create(IUser user, string title, string text)
        {
            var encryptedTitle = cryptoOperation.Encrypt(title);
            var encryptedText = cryptoOperation.Encrypt(text);

            var note = noteRepository.Create(user, encryptedTitle, encryptedText);

            return note;
        }

        public void Update(INote note, string title, string text)
        {
            var encryptedTitle = cryptoOperation.Encrypt(title);
            var encryptedText = cryptoOperation.Encrypt(text);

            noteRepository.Update(note, encryptedTitle, encryptedText);
        }
    }
}

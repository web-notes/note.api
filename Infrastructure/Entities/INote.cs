﻿namespace Infrastructure.Entities
{
    public interface INote
    {
        Guid Id { get; set; }

        string Title { get; set; }

        string Text { get; set; }

        DateTime Created { get; set; }

        DateTime Modified { get; set; }

        Guid UserFk { get; set; }

        IUser User { get; set; }
    }
}

using Encryption;
using Encryption.Options;
using Entity;
using FluentValidation;
using FluentValidation.AspNetCore;
using Infrastructure.Entities;
using Infrastructure.Operations;
using Infrastructure.Repositories;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Note.Controllers;
using Note.ModelBuilders;
using Operation.Note;
using Operation.User;
using Repository;
using System.Text;
using User.Controllers;
using User.Interfaces.Operations;
using User.Operations;
using User.Options;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddHttpContextAccessor();

RegisterEntities();
RegisterRepositories();
RegisterOperations();

ConfigureDbContext();
ConfigureAuthentication();

RegisterModelBuilders();

ConfigureOptions();

ConfigureValidation();

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().SetPreflightMaxAge(TimeSpan.MaxValue));

app.Run();

void RegisterEntities()
{
    builder.Services.AddTransient<INote, NoteEntity>();
    builder.Services.AddTransient<IUser, UserEntity>();
}

void RegisterRepositories()
{
    builder.Services.AddScoped<INoteRepository, NoteRepository>();
    builder.Services.AddScoped<IUserRepository, UserRepository>();
}

void RegisterOperations()
{
    builder.Services.AddScoped<IUserOperation, UserOperation>();
    builder.Services.AddScoped<IIdentityOperation, IdentityOperation>();
    builder.Services.AddScoped<ITokenOperation, JwtTokenOperation>();
    builder.Services.AddSingleton<ICryptoOperation, AES256CryptoOperation>();
    builder.Services.AddScoped<INoteOperation, NoteOperation>();
    builder.Services.AddSingleton<IHashOperation, Md5HashOperation>();
}

void RegisterModelBuilders()
{
    builder.Services.AddSingleton<NoteModelBuilder>();
}

void ConfigureDbContext()
{
    builder.Services.AddDbContext<NoteDbContext>(contextBuilder =>
    {
        var connString = builder.Configuration.GetConnectionString("Note");
        contextBuilder.UseSqlServer(connString);
    });
}

void ConfigureOptions()
{
    builder.Services.Configure<AesOptions>(builder.Configuration.GetSection(AesOptions.Aes));
    builder.Services.Configure<AuthOptions>(builder.Configuration.GetSection(nameof(AuthOptions)));
}

void ConfigureAuthentication()
{
    var secretKey = builder.Configuration.GetValue<string>("AuthOptions:KEY");

    builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = true,
            ValidIssuer = builder.Configuration.GetSection("AuthOptions:ISSUER").Value,
            ValidateAudience = true,
            ValidAudience = builder.Configuration.GetSection("AuthOptions:AUDIENCE").Value,
            ValidateLifetime = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey)),
            ValidateIssuerSigningKey = true,
        };
    });
}

void ConfigureValidation()
{
    builder.Services.AddFluentValidationAutoValidation();
    builder.Services.AddFluentValidationClientsideAdapters();
    builder.Services.AddValidatorsFromAssemblyContaining<UserController>();
    builder.Services.AddValidatorsFromAssemblyContaining<NoteController>();

    ValidatorOptions.Global.DefaultRuleLevelCascadeMode = CascadeMode.StopOnFirstFailure;
}
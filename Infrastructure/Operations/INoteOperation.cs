﻿using Infrastructure.Entities;

namespace Infrastructure.Operations
{
    public interface INoteOperation
    {
        INote Create(IUser user, string title, string text);

        void Update(INote note, string title, string text);
    }
}

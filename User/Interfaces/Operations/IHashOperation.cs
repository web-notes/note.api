﻿namespace User.Interfaces.Operations
{
    public interface IHashOperation
    {
        string Hash(string input);
    }
}

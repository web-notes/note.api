﻿namespace Encryption.Options
{
    public class AesOptions
    {
        public const string Aes = "Aes";

        public string Key { get; set; }
    }
}

﻿using Entity;
using Infrastructure.Entities;
using Infrastructure.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly NoteDbContext noteDbContext;

        private readonly IServiceProvider serviceProvider;

        public UserRepository(NoteDbContext noteDbContext, IServiceProvider serviceProvider)
        {
            this.noteDbContext = noteDbContext;
            this.serviceProvider = serviceProvider;
        }

        public IUser Create(string name, string passwordHash)
        {
            var entity = serviceProvider.GetRequiredService<IUser>();
            entity.Id = Guid.NewGuid();
            entity.Name = name;
            entity.PasswordHash = passwordHash;
            entity.Created = DateTime.UtcNow;

            var user = noteDbContext.User.Add((UserEntity)entity);

            noteDbContext.SaveChanges();

            return user.Entity;
        }

        public IUser Get(string name, string passwordHash)
        {
            return noteDbContext.User.FirstOrDefault(u => u.Name.Equals(name) && u.PasswordHash.Equals(passwordHash));
        }

        public IUser Get(string name)
        {
            return noteDbContext.User.FirstOrDefault(u => u.Name.Equals(name));
        }
    }
}

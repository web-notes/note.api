using Infrastructure.Operations;
using Infrastructure.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Note.ModelBuilders;
using Note.Models.Note;

namespace Note.Controllers
{
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class NoteController : ControllerBase
    {
        private readonly INoteRepository noteRepository;

        private readonly IUserOperation userOperation;

        private readonly INoteOperation noteOperation;

        private readonly NoteModelBuilder noteModelBuilder;

        public NoteController(INoteRepository noteRepository, IUserOperation userOperation, INoteOperation noteOperation, NoteModelBuilder noteModelBuilder)
        {
            this.noteRepository = noteRepository;
            this.userOperation = userOperation;
            this.noteOperation = noteOperation;
            this.noteModelBuilder = noteModelBuilder;
        }

        [HttpPost]
        public NoteModel Create(NoteCreateModel model)
        {
            var user = userOperation.CurrentUser;

            var note = noteOperation.Create(user, model.Title, model.Text);

            return noteModelBuilder.Build(note);
        }

        [HttpGet]
        public IEnumerable<NoteModel> Get()
        {
            var user = userOperation.CurrentUser;

            var notes = noteRepository.Get(user);

            return notes.Select(noteModelBuilder.Build).OrderByDescending(m => m.Modified);
        }

        [HttpPut]
        public NoteModel Update(NoteUpdateModel model)
        {
            var note = noteRepository.Get(model.Id);

            noteOperation.Update(note, model.Title, model.Text);

            return noteModelBuilder.Build(note);
        }

        [HttpDelete]
        public HttpResponseMessage Delete(NoteDeleteModel model)
        {
            var note = noteRepository.Get(model.Id);

            noteRepository.Delete(note);

            return new HttpResponseMessage(System.Net.HttpStatusCode.OK);
        }
    }
}
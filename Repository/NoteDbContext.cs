﻿using Entity;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class NoteDbContext : DbContext
    {
        public NoteDbContext(DbContextOptions<NoteDbContext> options) : base(options)
        {
        }

        public DbSet<NoteEntity> Note { get; set; }

        public DbSet<UserEntity> User { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<NoteEntity>()
                .HasOne(p => p.User as UserEntity);
        }
    }
}

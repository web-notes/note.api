﻿using System.Security.Claims;

namespace Infrastructure.Operations
{
    public interface IIdentityOperation
    {
        ClaimsIdentity? Get(string name, string passwordHash);
    }
}

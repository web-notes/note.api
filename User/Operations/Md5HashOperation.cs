﻿using System.Security.Cryptography;
using User.Interfaces.Operations;

namespace User.Operations
{
    public class Md5HashOperation : IHashOperation
    {
        public string Hash(string input)
        {
            using (MD5 md5 = MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                return Convert.ToHexString(hashBytes);
            }
        }
    }
}
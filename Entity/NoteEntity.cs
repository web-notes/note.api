﻿using Infrastructure.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity
{
    public class NoteEntity : INote
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Text { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        [ForeignKey("User")]
        public Guid UserFk { get; set; }
        public IUser User { get; set; }

        IUser INote.User
        {
            get => User;
            set
            {
                User = value as UserEntity;
            }
        }
    }
}
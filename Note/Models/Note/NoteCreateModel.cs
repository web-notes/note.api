﻿using Newtonsoft.Json;

namespace Note.Models.Note
{
    public class NoteCreateModel
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }
    }
}

﻿using Infrastructure.Entities;
using Infrastructure.Operations;
using Infrastructure.Repositories;
using Microsoft.AspNetCore.Http;

namespace Operation.User
{
    public class UserOperation : IUserOperation
    {
        private readonly IHttpContextAccessor httpContextAccessor;

        private readonly IUserRepository userRepository;

        public UserOperation(IHttpContextAccessor httpContextAccessor, IUserRepository userRepository)
        {
            this.httpContextAccessor = httpContextAccessor;
            this.userRepository = userRepository;
        }

        public IUser CurrentUser => userRepository.Get(GetCurrentUserName());

        private string GetCurrentUserName()
        {
            return httpContextAccessor.HttpContext.User.Identity.Name;
        }
    }

}

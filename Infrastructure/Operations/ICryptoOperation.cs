﻿namespace Infrastructure.Operations
{
    public interface ICryptoOperation
    {
        string Encrypt(string data);

        string Decrypt(string encoded);
    }
}

﻿using Infrastructure.Operations;
using Infrastructure.Repositories;
using System.Security.Claims;

namespace User.Operations
{
    public class IdentityOperation : IIdentityOperation
    {
        private readonly IUserRepository userRepository;

        public IdentityOperation(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        public ClaimsIdentity? Get(string name, string passwordHash)
        {
            var user = userRepository.Get(name, passwordHash);

            if (user == null) return null;

            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Name),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, string.Empty)
            };

            var claimsIdentity = new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;
        }
    }
}

﻿using Newtonsoft.Json;

namespace Note.Models.Note
{
    public class NoteUpdateModel
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }
    }
}

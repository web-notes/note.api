﻿using Infrastructure.Entities;
using Infrastructure.Operations;
using Note.Models.Note;

namespace Note.ModelBuilders
{
    public class NoteModelBuilder
    {
        private readonly ICryptoOperation cryptoOperation;

        public NoteModelBuilder(ICryptoOperation cryptoOperation)
        {
            this.cryptoOperation = cryptoOperation;
        }

        public NoteModel Build(INote note)
        {
            return new NoteModel
            {
                Id = note.Id,
                Title = cryptoOperation.Decrypt(note.Title),
                Text = cryptoOperation.Decrypt(note.Text),
                Modified = note.Modified
            };
        }
    }
}

﻿using Infrastructure.Entities;

namespace Infrastructure.Repositories
{
    public interface INoteRepository
    {
        INote Create(IUser user, string title, string text);

        IEnumerable<INote> Get(IUser user);

        INote Get(Guid id);

        void Update(INote note, string title, string text);

        void Delete(INote note);
    }
}

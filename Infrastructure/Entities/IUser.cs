﻿namespace Infrastructure.Entities
{
    public interface IUser
    {
        Guid Id { get; set; }

        string Name { get; set; }

        string PasswordHash { get; set; }

        DateTime Created { get; set; }
    }
}

﻿using Newtonsoft.Json;

namespace Note.Models.Note
{
    public class NoteDeleteModel
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }
    }
}

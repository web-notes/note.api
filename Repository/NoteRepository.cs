﻿using Entity;
using Infrastructure.Entities;
using Infrastructure.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Repository
{
    public class NoteRepository : INoteRepository
    {
        private readonly NoteDbContext noteDbContext;

        private readonly IServiceProvider serviceProvider;

        public NoteRepository(IServiceProvider serviceProvider, NoteDbContext noteDbContext)
        {
            this.noteDbContext = noteDbContext;
            this.serviceProvider = serviceProvider;
        }

        public INote Create(IUser user, string title, string text)
        {
            var entity = serviceProvider.GetRequiredService<INote>();
            entity.Id = Guid.NewGuid();
            entity.Title = title;
            entity.Text = text;
            entity.User = user;
            entity.Created = DateTime.UtcNow;
            entity.Modified = DateTime.UtcNow;

            var note = noteDbContext.Note.Add((NoteEntity)entity);
            noteDbContext.Entry(entity.User).State = Microsoft.EntityFrameworkCore.EntityState.Unchanged;

            noteDbContext.SaveChanges();

            return note.Entity;
        }

        public IEnumerable<INote> Get(IUser user)
        {
            return noteDbContext.Note.Where(n => n.User.Equals(user));
        }

        public INote Get(Guid id)
        {
            return noteDbContext.Note.Find(id);
        }

        public void Update(INote note, string title, string text)
        {
            note.Title = title;
            note.Text = text;
            note.Modified = DateTime.UtcNow;

            noteDbContext.SaveChanges();
        }

        public void Delete(INote note)
        {
            noteDbContext.Remove(note);
            noteDbContext.SaveChanges();
        }
    }
}

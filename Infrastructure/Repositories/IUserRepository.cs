﻿using Infrastructure.Entities;

namespace Infrastructure.Repositories
{
    public interface IUserRepository
    {
        IUser Create(string name, string passwordHash);

        IUser Get(string name, string passwordHash);

        IUser Get(string name);
    }
}

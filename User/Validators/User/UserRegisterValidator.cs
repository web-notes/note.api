﻿using FluentValidation;
using Infrastructure.Repositories;
using User.Models.User;

namespace User.Validators.User
{
    public class UserRegisterValidator : AbstractValidator<UserRegisterModel>
    {
        private readonly IUserRepository userRepository;

        public UserRegisterValidator(IUserRepository userRepository)
        {
            this.userRepository = userRepository;

            RuleFor(model => model)
                .Custom(ValidateName)
                .Custom(ValidatePassword)
                .Custom(CheckDuplicates);
        }

        private void ValidateName(UserRegisterModel model, ValidationContext<UserRegisterModel> context)
        {
            if (string.IsNullOrWhiteSpace(model.Name))
                context.AddFailure(nameof(model.Name), "Name is required");
        }

        private void ValidatePassword(UserRegisterModel model, ValidationContext<UserRegisterModel> context)
        {
            if (string.IsNullOrWhiteSpace(model.Password))
                context.AddFailure(nameof(model.Password), "Password is required");
        }

        private void CheckDuplicates(UserRegisterModel model, ValidationContext<UserRegisterModel> context)
        {
            var existingUser = userRepository.Get(model.Name);
            if (existingUser != null)
                context.AddFailure(nameof(model.Name), "User with specified name already exists");
        }
    }
}

﻿using Infrastructure.Entities;

namespace Infrastructure.Operations
{
    public interface IUserOperation
    {
        IUser CurrentUser { get; }
    }
}

﻿using Encryption.Constants;
using Encryption.Options;
using Infrastructure.Operations;
using Microsoft.Extensions.Options;
using System.Security.Cryptography;
using System.Text;

namespace Encryption
{
    public class AES256CryptoOperation : ICryptoOperation
    {
        private readonly byte[] key;

        public AES256CryptoOperation(IOptions<AesOptions> aesOptions)
        {
            key = Encoding.UTF8.GetBytes(aesOptions.Value.Key);
        }

        public string Encrypt(string data)
        {
            byte[] encrypted;

            using (var aesAlg = Aes.Create())
            {
                aesAlg.GenerateIV();
                var iv = aesAlg.IV;

                var encryptor = aesAlg.CreateEncryptor(key, iv);
                
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(data);
                        }
                        encrypted = msEncrypt.ToArray();
                        encrypted = iv.Concat(encrypted).ToArray();
                    }
                }
            }

            return Convert.ToBase64String(encrypted);
        }

        public string Decrypt(string encoded)
        {
            string decrypted;

            using (var aesAlg = Aes.Create())
            {
                var encodedBytes = Convert.FromBase64String(encoded);
                var iv = encodedBytes.Take(AesConstants.DefaultIvLength).ToArray();
                var data = encodedBytes.Skip(AesConstants.DefaultIvLength).ToArray();

                var decryptor = aesAlg.CreateDecryptor(key, iv);

                using (MemoryStream msDecrypt = new MemoryStream(data))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            decrypted = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }

            return decrypted;
        }
    }
}

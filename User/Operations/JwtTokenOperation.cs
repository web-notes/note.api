﻿using Infrastructure.Operations;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using User.Options;

namespace User.Operations
{
    public class JwtTokenOperation : ITokenOperation
    {
        private readonly AuthOptions authOptions;

        public JwtTokenOperation(IOptions<AuthOptions> authOptions)
        {
            this.authOptions = authOptions.Value;
        }

        public string Generate(ClaimsIdentity identity, out DateTime expirationDate)
        {
            var now = DateTime.UtcNow;
            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(authOptions.Key));

            expirationDate = now.Add(TimeSpan.FromMinutes(authOptions.Lifetime));

            var jwt = new JwtSecurityToken(authOptions.Issuer, authOptions.Audience, identity.Claims, now, expirationDate, new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256));

            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }
    }
}

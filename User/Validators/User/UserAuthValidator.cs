﻿using FluentValidation;
using Infrastructure.Operations;
using User.Interfaces.Operations;
using User.Models.User;

namespace User.Validators.User
{
    public class UserAuthValidator : AbstractValidator<UserAuthModel>
    {
        private readonly IHashOperation hashOperation;

        private readonly IIdentityOperation identityOperation;

        public UserAuthValidator(IHashOperation hashOperation, IIdentityOperation identityOperation)
        {
            this.identityOperation = identityOperation;
            this.hashOperation = hashOperation;

            RuleFor(model => model)
                .Custom(ValidateIdentity);
        }

        private void ValidateIdentity(UserAuthModel model, ValidationContext<UserAuthModel> context)
        {
            var passwordHash = hashOperation.Hash(model.Password);

            var identity = identityOperation.Get(model.Name, passwordHash);
            if (identity == null)
                context.AddFailure(string.Empty, "Cannot find a user with specified credentials");
        }

    }
}
